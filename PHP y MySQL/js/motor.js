$(document).ready(init);

function init()
{
	$("#btn_enviar").click(procesar);
	$("#btn_login").click(login);
}

function procesar()
{
	$.post("php/procesar.php", $("#form").serialize()).done(function (data){comprobar(data);})
}

function login()
{
	$.post("php/login.php", $("#register").serialize()).done(function (data){logearse(data);})

}

function comprobar(data)
{
	if (data)
	{
		$("#secundario").hide(2000, acceso);
	}
	else
	{
		$("#respuesta").html("Your user or password is not correct");
	}
}

function logearse(data)
{
	if (data)
	{
		$("#new_respuesta").html("You have successfully registered");
	}
}

function acceso()
{
	$("#secundario").html("<h3>Bienvenido</h3>");
	$("#secundario").show(2000);
}